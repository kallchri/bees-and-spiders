function camera = ChangeOrientation(camera,i)

%         camera(i,5) = camera(i,5) - 1; % camera(k,6) is the yaw angle
%         camera(i,6) = camera(i,6) - 1; % camera(k,6) is the yaw angle
                if camera(i,5) > 0
                    camera(i,5) = camera(i,5) - 1; % camera(k,6) is the yaw angle
                end
                
                if camera(i,5) < 0
                    camera(i,5) = camera(i,5) + 1; % camera(k,6) is the yaw angle
                    %target_angles(:,:) = target_angles_previous(:,:);
                end
  
                
                if camera(i,6) > 0
                    camera(i,6) = camera(i,6) - 1; % camera(k,6) is the yaw angle
                end
                
                if camera(i,6) < 0
                    camera(i,6) = camera(i,6) + 1; % camera(k,6) is the yaw angle
                    %target_angles(:,:) = target_angles_previous(:,:); 
                end
end