function [target_angles,camera,done] = FindTargetAngles(target_angles,target_angles_previous,camera,done,N,M)

    for(i=1:N)
        for (j=1:M)
            if (target_angles_previous(1,j) <  target_angles(1,j))

                if camera(i,5) > 0
                    camera(i,5) = camera(i,5) - 1; % camera(k,6) is the yaw angle
                end
                
                if camera(i,5) < 0
                    camera(i,5) = camera(i,5) + 1; % camera(k,6) is the yaw angle
                    %target_angles(:,:) = target_angles_previous(:,:);
                end
                
                if camera(i,5) == 0
                    
                end
                
                
            elseif (target_angles_previous(1,j) >  target_angles(1,j))
                
                if camera(i,6) > 0
                    camera(i,6) = camera(i,6) - 1; % camera(k,6) is the yaw angle
                end
                
                if camera(i,6) < 0
                    camera(i,6) = camera(i,6) + 1; % camera(k,6) is the yaw angle
                    %target_angles(:,:) = target_angles_previous(:,:); 
                end
                
                 if camera(i,6) == 0
                    
                end
            else % if they are equal
                
                done = done + 1;
                %disp('done');
                break;
                % put a flag to stop 

            end
        end
    end
end