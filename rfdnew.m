clear;clc;
run('graph');
N = 6; % 10 nodes in graph -- nodes correspond to the possible soultions - the internal angles of the polygon - the available cameras to solve the problem
initial_alt = 10; 
altitude(1:N-1) = initial_alt; % all nodes are initialised at the same altitude apart from the dst node
altitude(N) = 0;
K = 100; % number of drops in the system
drops =  0 ;
maxit = 100; % maximum number of iterations
cnt = 1;
cnt_it = 0;
current = 1; % current node
next = 2 ; % next node 
paths = 1;
sediment_flag = 0;
old = 0;
new = 1;
gradient = zeros(N);
probability = NaN(N); 
while ( (~same_paths(old,new,current,N) )  & (cnt_it <= maxit)  ) % loop for algorithm -> this terminates the algorithm 
    if ( (next == (N+1)) | (sediment_flag == 1))
        current = 1;
        next = 2;
        cnt = cnt + 1;
        drops = 0;
        old = paths;
        paths = 1;
        sediment_flag = 0;
    end
    while ( (current ~= N) & (sediment_flag == 0) ) % loop for drop
        suma = 0;
        i = current;
        j = next;
        drops = drops + i; 
        pathlength = drops;
        gradient = zeros(N);
        probability = NaN(N);
        erosion = 0;
        while (  j<=N ) % loop for neighbours 
            if (adj_matrix(i,j) == 1)
                gradient(i,j) = (altitude(j) - altitude(i)) / (j-i) ; 
                for (cost_cnt = 1:(N-i))
                    suma = suma + cost(i,(cost_cnt+i));
                end
                distance_to_goal = N - j; %number of hops from next node to the goal
                if ( altitude(i) > altitude(j) ) 
                    probability(i,j) = abs((gradient(i,j))) ./ (suma * distance_to_goal) ; 
                    erosion = abs(gradient(i,j)) ./ ((N-1)*K * pathlength );
                elseif ( altitude(i) < altitude(j) )
                    probability(i,j) =  ((1 ./ (gradient(i,j))) ./ (suma * distance_to_goal)) ;
                    erosion =  1 ./ (((gradient(i,j)) * (N-1)*K * pathlength));
                    
                else
                    probability(i,j) = ( 1 / (suma * distance_to_goal)) ;
                    erosion = 1 / ((N-1)*K * pathlength );
                end
            end
            j = j+1 ;
        end
        
        non_zero = (gradient(current,:) ~= 0);
        positive = (gradient(current,:) > 0);
        
        if ( sum(non_zero) == sum(positive) & altitude(current) < initial_alt ) 
            if ( current >= N)
                altitude(N) = altitude(N);
            else
                altitude(current) = altitude(current) + abs(erosion);
            end
            sediment_flag = 1;
        else
			[max_p,max_index] = max(probability(current,:)) ; 
			current =  max_index ;
			paths(end+1) = current;
			altitude(i) = altitude(i) - abs(erosion) ; 
			sediment_flag = 0;
        end
        next = current + 1 ;
    end
    new = paths;
    cnt_it = cnt_it + 1;
end 
disp(sprintf('the path chosen for this graph is:\n'));
disp(paths);

run('new2');