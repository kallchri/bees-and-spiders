clear;

%cameras co-ordinates [x, y, z, �, �, �, f] z=3 �=0 f=3.6mm 
camera = [0 16 3 0 70 70 3.6;
         15 16 3 0 70 70 3.6;
         20 8 3 0 70 70 3.6;
         18 0 3 0 70 70 3.6;
         15 0 3 0 70 70 3.6;
         0 0 3 0 70 70 3.6];

% % target point co-ordinates [x, y, z, �, �, �, f] z=2 �,�,�,f=0  
% T = [0 8 2 0 0 0 0;
%     10 12 2 0 0 0 0;
%     15 12 2 0 0 0 0;
%     18 8 2 0 0 0 0;
%     12 2 2 0 0 0 0;
%     2 2 2 0 0 0 0];

T = [0 8 2 0 0 0 0;
    10 12 2 0 0 0 0;
    15 12 2 0 0 0 0;
    18 8 2 0 0 0 0;
    12 2 2 0 0 0 0;
    2 2 2 0 0 0 0;
    2 8.5 2 0 0  0 1;
    12 5.5 2 0 0 0 1];

N = 6; % max cameras in layout
M = 8; % total number of target points
ar = 4/3; % aspect ratio
f = 3.6; % focal length in mm ******METERS OR MM??? *****
height = 22.9; % in meters
width = 17.4; % in meters
d_width = sqrt(height^2 + width^2); %diagonal width of the camera

Dmax = 15.2; % Dmax is the maximum depth of field in meters
 
Dmin = (ar * f) / d_width * 0.001;  % Dmin is the minimum depth of field give by the equation Dmin = (aspect_ratio * focal_length) / diagonal_width_of_image

omega = 72;

phi(N,M) = 0;
phideg(N,M) = 0;
[phideg(1,4),phideg(2,1),phideg(2,2),phideg(2,6),phideg(2,8),phideg(3,2),phideg(3,3),phideg(4,1),phideg(4,2),phideg(4,3),phideg(4,5),phideg(4,6),phideg(5,1),phideg(5,2),phideg(5,5),phideg(5,6),phideg(5,7),phideg(5,8)] = deal(Inf);



phi(1:N,1:M) = deg2rad(phideg(1:N,1:M));

observe2(N,M) = 0;
observe2(1,:) = norm(T(1,:))*cos(phi(1,:));
observe2(2,:) = norm(T(2,:))*cos(phi(2,:));
observe2(3,:) = norm(T(3,:))*cos(phi(3,:));
observe2(4,:) = norm(T(4,:))*cos(phi(4,:));
observe2(5,:) = norm(T(5,:))*cos(phi(5,:));
observe2(6,:) = norm(T(6,:))*cos(phi(6,:));


out_of_range(N,M) = 0;
[out_of_range(1,4),out_of_range(2,1),out_of_range(2,2),out_of_range(2,6),out_of_range(3,2),out_of_range(3,3),out_of_range(4,1),out_of_range(4,2),out_of_range(4,3),out_of_range(4,5),out_of_range(4,6),out_of_range(5,1),out_of_range(5,2),out_of_range(5,5),out_of_range(5,6),] = deal(1);


loops = 0;
done = 0;

target_angles_previous(2,M) = 0;
target_angles_previous(1,:) = 0;
target_angles_previous(2,:) = 0;

% co-ordinates for rectangle of high density crowd - find the middle point
% and work with that as the new target point 

td1 = [1 12; 3 12; 1 5; 3 5];
td2 = [10 7; 14 7; 10 4; 14 4];

td1_mid = [(td1(2,1)+td1(3,1))/2  (td1(2,2)+td1(3,2))/2];
td2_mid = [(td2(2,1)+td2(3,1))/2  (td2(2,2)+td2(3,2))/2];

while (loops <= 100 && done == 0)
    
    [phideg,out_of_range,camera] = FindPhiDeg(phideg,camera,T,observe2,Dmin,Dmax,out_of_range,N,M);
    
    [angle, place] = min(phideg(:,1:M));
        
    target_angles(1,:) = angle; % gives smallest angle for targets -> each column is the corresponding target aka column 1 is T1
    target_angles(2,:) = place; % gives the camera that gives the smallest angle aka camera1 sees T1 with the smallest angle / camera2 sees T3 etc
    
    [target_angles,camera,done] = FindTargetAngles(target_angles,target_angles_previous,camera,done,N,M);

    targetopti = target_angles(1,:);
    cameraopti = unique(target_angles(2,:))';
    camera(:,:) = camera(:,:);
    
    target_angles_previous(1,:) = target_angles(1,:);
    target_angles_previous(2,:) = target_angles(2,:);
    
    loops = loops + 1;
 
end

disp('the minimum angles and corresponding cameras are:');
disp(target_angles);

